/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.jsonmapper.test.launcher;

import java.util.Arrays;

import org.slf4j.bridge.SLF4JBridgeHandler;
import org.testng.TestNG;

/**
 * @author indroneel
 *
 */

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		//setup slf4j as the underlying logging mechanism.
		SLF4JBridgeHandler.removeHandlersForRootLogger();
		SLF4JBridgeHandler.install();

		TestNG testng = new TestNG();
		testng.setTestSuites(Arrays.asList("testng.xml"));
		testng.setVerbose(4);
		testng.setOutputDirectory("reports");
		testng.run();
	}
}
