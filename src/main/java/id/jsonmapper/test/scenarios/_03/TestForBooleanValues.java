/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.jsonmapper.test.scenarios._03;

import java.io.FileInputStream;

import org.testng.Assert;
import org.testng.annotations.Test;

import id.jsonmapper.decode.Json2Object;

/**
 * @author indroneel
 *
 */

public class TestForBooleanValues {

	@Test(description = "missing json field populates to a value of false")
	public void _01() throws Exception {
		FileInputStream fis = new FileInputStream("data/_03/switch01.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		Switch sw = (Switch) j2o.convert(Switch.class);

		Assert.assertNotNull(sw);
		Assert.assertFalse(sw.isValue());
	}

	@Test(description = "json boolean field with value of true is loaded correctly")
	public void _02() throws Exception {
		FileInputStream fis = new FileInputStream("data/_03/switch02.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		Switch sw = (Switch) j2o.convert(Switch.class);

		Assert.assertNotNull(sw);
		Assert.assertTrue(sw.isValue());
	}

	@Test(description = "json string field with value of 'true' is loaded correctly")
	public void _03() throws Exception {
		FileInputStream fis = new FileInputStream("data/_03/switch03.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		Switch sw = (Switch) j2o.convert(Switch.class);

		Assert.assertNotNull(sw);
		Assert.assertTrue(sw.isValue());
	}

	@Test(description = "json string field with value other than 'true' populates to a value of false")
	public void _04() throws Exception {
		FileInputStream fis = new FileInputStream("data/_03/switch04.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		Switch sw = (Switch) j2o.convert(Switch.class);

		Assert.assertNotNull(sw);
		Assert.assertFalse(sw.isValue());
	}

	@Test(description = "missing json field populates to a Boolean value of null")
	public void _05() throws Exception {
		FileInputStream fis = new FileInputStream("data/_03/switch01.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		BigSwitch sw = (BigSwitch) j2o.convert(BigSwitch.class);

		Assert.assertNotNull(sw);
		Assert.assertNull(sw.getValue());
	}

	@Test(description = "json boolean field with value of true populates to a Boolean value of TRUE")
	public void _06() throws Exception {
		FileInputStream fis = new FileInputStream("data/_03/switch02.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		BigSwitch sw = (BigSwitch) j2o.convert(BigSwitch.class);

		Assert.assertNotNull(sw);
		Assert.assertEquals(sw.getValue(), Boolean.TRUE);
	}

	@Test(description = "json string field with value of 'true' populates to a Boolean value of TRUE")
	public void _07() throws Exception {
		FileInputStream fis = new FileInputStream("data/_03/switch03.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		BigSwitch sw = (BigSwitch) j2o.convert(BigSwitch.class);

		Assert.assertNotNull(sw);
		Assert.assertEquals(sw.getValue(), Boolean.TRUE);
	}

	@Test(description = "json boolean field with value of false populates to a Boolean value of FALSE")
	public void _08() throws Exception {
		FileInputStream fis = new FileInputStream("data/_03/switch05.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		BigSwitch sw = (BigSwitch) j2o.convert(BigSwitch.class);

		Assert.assertNotNull(sw);
		Assert.assertEquals(sw.getValue(), Boolean.FALSE);
	}

	@Test(description = "json string field with value of 'false' populates to a Boolean value of FALSE")
	public void _09() throws Exception {
		FileInputStream fis = new FileInputStream("data/_03/switch06.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		BigSwitch sw = (BigSwitch) j2o.convert(BigSwitch.class);

		Assert.assertNotNull(sw);
		Assert.assertEquals(sw.getValue(), Boolean.FALSE);
	}

	@Test(description = "json string field with value other than 'true' or 'false' populates to a Boolean value of null")
	public void _10() throws Exception {
		FileInputStream fis = new FileInputStream("data/_03/switch04.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		BigSwitch sw = (BigSwitch) j2o.convert(BigSwitch.class);

		Assert.assertNotNull(sw);
		Assert.assertNull(sw.getValue());
	}
}
