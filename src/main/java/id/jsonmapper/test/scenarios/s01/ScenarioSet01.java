/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.jsonmapper.test.scenarios.s01;

import java.io.FileInputStream;

import org.testng.Assert;
import org.testng.annotations.Test;

import id.jsonmapper.decode.Json2Object;

/**
 * @author indroneel
 *
 */

public class ScenarioSet01 {

	@Test
	public void test01() throws Exception {
		FileInputStream fis = new FileInputStream("data/s01/fullname01.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		FullName fname = (FullName) j2o.convert(FullName.class);

		Assert.assertNotNull(fname);
		Assert.assertEquals(fname.getFirstName(), "Randon");
		Assert.assertEquals(fname.getMiddleName(), "x");
		Assert.assertEquals(fname.getLastName(), "Diesel");
	}

	@Test
	public void test02() throws Exception {
		FileInputStream fis = new FileInputStream("data/s01/fullname02.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		FullName fname = (FullName) j2o.convert(FullName.class);

		Assert.assertNotNull(fname);
		Assert.assertEquals(fname.getFirstName(), "Randon");
		Assert.assertNull(fname.getMiddleName());
		Assert.assertEquals(fname.getLastName(), "Diesel");
	}

	@Test
	public void test03() throws Exception {
		FileInputStream fis = new FileInputStream("data/s01/fullname03.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		FullName fname = (FullName) j2o.convert(FullName.class);

		Assert.assertNotNull(fname);
		Assert.assertEquals(fname.getFirstName(), "Randon");
		Assert.assertEquals(fname.getMiddleName(), "x");
		Assert.assertNull(fname.getLastName());
	}

	@Test
	public void test05() throws Exception {
		FileInputStream fis = new FileInputStream("data/s01/fullname05.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		FullName fname = (FullName) j2o.convert(FullName.class);

		Assert.assertNotNull(fname);
		Assert.assertEquals(fname.getFirstName(), "Randon");
		Assert.assertNull(fname.getMiddleName());
		Assert.assertEquals(fname.getLastName(), "Diesel");
	}
}
