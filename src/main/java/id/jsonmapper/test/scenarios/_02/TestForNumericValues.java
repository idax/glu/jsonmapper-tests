/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.jsonmapper.test.scenarios._02;

import java.io.FileInputStream;

import org.testng.Assert;
import org.testng.annotations.Test;

import id.jsonmapper.decode.Json2Object;

/**
 * @author indroneel
 *
 */

public class TestForNumericValues {

	@Test(description = "missing json field populates to an int value of 0")
	public void _01() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length01.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		IntLength len = (IntLength) j2o.convert(IntLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getValue(), 0);
	}

	@Test(description = "missing json field populates to an Integer value of null")
	public void _02() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length01.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		IntLength len = (IntLength) j2o.convert(IntLength.class);

		Assert.assertNotNull(len);
		Assert.assertNull(len.getBigValue());
	}

	@Test(description = "numeric json field populates to an int value correctly")
	public void _03() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length02.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		IntLength len = (IntLength) j2o.convert(IntLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getValue(), 100);
	}

	@Test(description = "numeric json field populates to an Integer value correctly")
	public void _04() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length02.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		IntLength len = (IntLength) j2o.convert(IntLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getBigValue(), Integer.valueOf(100));
	}

	@Test(description = "json string field that is numeric populates to an int value correctly")
	public void _05() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length03.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		IntLength len = (IntLength) j2o.convert(IntLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getValue(), 100);
	}

	@Test(description = "json string field that is numeric populates to an Integer value correctly")
	public void _06() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length03.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		IntLength len = (IntLength) j2o.convert(IntLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getBigValue(), Integer.valueOf(100));
	}

	@Test(description = "json string field that is alphanumeric populates to an int value of 0")
	public void _07() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length04.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		IntLength len = (IntLength) j2o.convert(IntLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getValue(), 0);
	}

	@Test(description = "json string field that is alphanumeric populates to an Integer value of null")
	public void _08() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length04.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		IntLength len = (IntLength) j2o.convert(IntLength.class);

		Assert.assertNotNull(len);
		Assert.assertNull(len.getBigValue());
	}

	////

	@Test(description = "missing json field populates to a float value of 0")
	public void _09() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length01.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		FloatLength len = (FloatLength) j2o.convert(FloatLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getValue(), 0);
	}

	@Test(description = "missing json field populates to a Float value of null")
	public void _10() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length01.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		FloatLength len = (FloatLength) j2o.convert(FloatLength.class);

		Assert.assertNotNull(len);
		Assert.assertNull(len.getBigValue());
	}

	@Test(description = "numeric json field populates to a float value correctly")
	public void _11() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length02.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		FloatLength len = (FloatLength) j2o.convert(FloatLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getValue()*100, 10021);
	}

	@Test(description = "numeric json field populates to a Float value correctly")
	public void _12() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length02.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		FloatLength len = (FloatLength) j2o.convert(FloatLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getBigValue(), Float.valueOf(100.21f));
	}

	@Test(description = "json string field that is numeric populates to a float value correctly")
	public void _13() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length03.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		FloatLength len = (FloatLength) j2o.convert(FloatLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getValue() * 100, 10021);
	}

	@Test(description = "json string field that is numeric populates to a Float value correctly")
	public void _14() throws Exception {
		FileInputStream fis = new FileInputStream("data/_02/length03.json");
		Json2Object j2o = new Json2Object(fis);
		fis.close();
		FloatLength len = (FloatLength) j2o.convert(FloatLength.class);

		Assert.assertNotNull(len);
		Assert.assertEquals(len.getBigValue(), Float.valueOf(100.21f));
	}
}
